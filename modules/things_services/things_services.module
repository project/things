<?php

/*
 * Implementation of hook_permission
 */
function things_services_permission() {
  return array(
    'create things services' => array(
      'title' => t('Create Things services'), 
      'description' => t('Create Things services'),
    ),
    'update things services' => array(
      'title' => t('Update Things services'), 
      'description' => t('Update Things services'),
    ),
    'delete things services' => array(
      'title' => t('Delete Things services'), 
      'description' => t('Delete Things services'),
    ),
    'retrieve things services' => array(
      'title' => t('Retrieve Things services'), 
      'description' => t('Retrieve Things services'),
    ),
  );
}

/*
 * Implementation of hook_services_resources().
 */
function things_services_services_resources() {
  $basic_args = array(
    array (
      'name' => 'owner',
      'optional' => TRUE,
      'source' => array('data' => 'owner'),
      'description' => 'Things owner', 
      'type' => 'string',
    ),
    array (
      'name' => 'thingsname',
      'optional' => TRUE,
      'source' => array('data' => 'thingsname'),
      'description' => 'Things name',
      'type' => 'string',
    ),
    array (
      'name' => 'updater',
      'optional' => TRUE,
      'source' => array('data' => 'updater'),
      'description' => 'Things updater',
      'type' => 'int',
    ),
    array (
      'name' => 'identifier',
      'optional' => FALSE, 
      'source' => array('data' => 'identifier'),
      'description' => 'Identifier', 
      'type' => 'string',
    ),
    array (
      'name' => 'identifiertype',
      'optional' => FALSE,
      'source' => array('data' => 'identifiertype'),
      'description' => 'Identifier type', 
      'type' => 'string',
    ),
  );

  $resources = array(
    'things' => array (
      'create' => array(
        'help' => 'Create things',
        'access callback' => '_things_create_access',
        'args' => array (
          array (
            'name' => 'owner',
            'optional' => TRUE,
            'source' => array('data' => 'owner'),
            'description' => 'Things owner', 
            'type' => 'string',
          ),
          array (
            'name' => 'thingsname',
            'optional' => TRUE,
            'source' => array('data' => 'thingsname'),
            'description' => 'Things name',
            'type' => 'string',
            'default value' => 'New things',
          ),
          array (
            'name' => 'updater',
            'optional' => TRUE,
            'source' => array('data' => 'updater'),
            'description' => 'Things updater',
            'type' => 'int',
          ),
          array (
            'name' => 'identifier',
            'optional' => FALSE, 
            'source' => array('data' => 'identifier'),
            'description' => 'Identifier', 
            'type' => 'string',
          ),
          array (
            'name' => 'identifiertype',
            'optional' => FALSE,
            'source' => array('data' => 'identifiertype'),
            'description' => 'Identifier type', 
            'type' => 'string',
          ),
          array (
            'name' => 'type',
            'optional' => FALSE,
            'source' => array('data' => 'type'),
            'description' => 'Things type', 
            'type' => 'string',
            'default value' => 'android_device',
          ),
        ),
        'callback' => '_things_create',
      ),
      'update' => array(
        'help' => 'Update things',
        'access callback' => '_things_update_access',
        'args' => array (
          array (
            'name' => 'id',
            'optional' => FALSE, 
            'source' => array('path' => 0),
            'description' => 'Things id', 
            'type' => 'int',
          ),
          array (
            'name' => 'thingsname',
            'optional' => TRUE,
            'source' => array('data' => 'thingsname'),
            'description' => 'Things name',
            'type' => 'string',
          ),
          array (
            'name' => 'owner',
            'optional' => TRUE,
            'source' => array('data' => 'owner'),
            'description' => 'Things owner', 
            'type' => 'string',
          ),
          array (
            'name' => 'updater',
            'optional' => TRUE,
            'source' => array('data' => 'updater'),
            'description' => 'Things updater',
            'type' => 'int',
          ),
          array (
            'name' => 'identifier',
            'optional' => FALSE, 
            'source' => array('data' => 'identifier'),
            'description' => 'Identifier', 
            'type' => 'string',
          ),
          array (
            'name' => 'identifiertype',
            'optional' => FALSE,
            'source' => array('data' => 'identifiertype'),
            'description' => 'Identifier type', 
            'type' => 'string',
          ),
        ),
        'callback' => '_things_update',
      ),
      'delete' => array(
        'help' => 'Delete things',
        'access callback' => '_things_delete_access',
        'args' => array (
          array (
            'name' => 'id',
            'optional' => FALSE, 
            'source' => array('path' => 0),
            'description' => 'Things id', 
            'type' => 'int',
          ),
        ),
        'callback' => '_things_delete',
      ),
      'retrieve' => array(
        'help' => 'Retrieve things',
        'access callback' => '_things_retrieve_access',
        'args' => array (
          array (
            'name' => 'id',
            'optional' => FALSE, 
            'source' => array('path' => 0),
            'description' => 'Things id', 
            'type' => 'int',
          ),
        ),
        'callback' => '_things_retrieve',
      ),
    ),
  );

  return $resources;
}

function _things_create_access() {
  return user_access('create things services');
}
function _things_update_access() {
  return user_access('update things services');
}
function _things_delete_access() {
  return user_access('delete things services');
}
function _things_retrieve_access() {
  return user_access('retrieve things services');
}
function _update_android_device($android, $name, $owner, $identifier_type, $identifier, $updater) {
  $error = NULL;

  // Handle name
  if (isset($name) && $android->name !== $name) {
    $android->name = $name;
  }

  // Handle owner
  // TODO: support multiple owners
  $error = _handle_owner($android, $owner);

  if (!$error) {
    // Handle identifier
    if ($identifier_type === "field_device_id") {
      $error = _handle_one_identifier($android, $identifier_type, $identifier);
    } else if ($identifier_type === "field_gcm_id") {
      $error = _handle_one_identifier($android, $identifier_type, $identifier);
    } else {
      $error = t('Identifier type @type not supported', array('@type' => $identifier_type));
    }
  }

  // Handle updater
  if (!$error) {
    $error = _handle_updater($android, $updater);
  }
  return $error;
}

function _android_device_create($type, $name, $owner, $updater, $identifier, $identifier_type) {
  // Check if the "thing" already exist using a View. Update if it does instead of create
  $display = 'default';
  $view = views_get_view('get_things_by_identifer');
  if ($view != null) {
    $view->set_display($display);
    $view->set_items_per_page(0);

    if ($identifier_type === "field_device_id") {
      $view->set_arguments(array( $identifier, "all" ));
    } else {
      $view->set_arguments(array( "all", $identifier ));
    }
    $view->execute();
    $response = $view->result;
  }
  $android = NULL;
  $error = NULL;
  
  if ($response && isset($response[0]) && $response[0]->things_id != 0) {
    // update
    $thing_id = $response[0]->things_id;
    return _android_device_update($thing_id, $name, $owner, $updater, $identifier, $identifier_type);
  } else {
    // create
    $android = _create_things($type, $name);

    if ($android) {
      $error = _update_android_device($android, $name, $owner, $identifier_type, $identifier, $updater);
    } else {
      $error = t('Failed to create Android device');
    }

    if ($android && !$error) {
      return (object)array('id' => $android->things_id);
    } else {
      return services_error(t('Failed to register Android device'), 406, $error);
    }
  }
}

function _android_device_update($id, $things_name, $owner, $updater, $identifier, $identifier_type = NULL) {
  $android = NULL;
  $error = NULL;

  $id = intval($id);

  // has ID - try to fetch the Things
  $android = things_load($id);

  if ($android) {
    if ($android->type == "android_device") {
      $error = _update_android_device($android, $things_name, $owner, $identifier_type, $identifier, $updater);
    } else {
      $return = services_error(t('Things type \'@type\' is currently not supported', array('@type' => $android->type)), 406);
    }

  } else {
    $error = t('Failed to load Android device with id @id', array('@id' => $id));
  }

  if ($android && !$error) {
    return (object)array('id' => $android->things_id);
  } else {
    return services_error(t('Failed to register Android device with id @id', array('@id' => $id)), 406, $error);
  }

}

function _handle_updater($android, $updater_id = 0) {
  if ($updater_id == 0) {
    $updater_id = $android->things_id;
  }

  if ($updater_id != 0) {
    $needs_update = FALSE;
    $temp = $android->field_updater;
    if (isset($temp['und'][0]['target_id'])) {
      $owner_id = $temp['und'][0]['target_id'];

      if ($owner_id != $owner_uid) {
        $needs_update = TRUE;
      }
    } else {
      $needs_update = TRUE;
    }
    if ($needs_update) {
      // check if updater exists
      if ($updater_id != $android->things_id) {
        $updater = things_load($updater_id);
        if (!$updater) {
          $error = t('Failed to load things updater with things id @id', array('@id' => $updater_id));
        }
      }
      if (!$error) {
        $android->field_updater = reference_field($updater_id);
      }
    }

    if (!$error) {
      things_save($android);
    }
  } else {
    watchdog('things services', "No updater to update");
  }

  return $error;

}

function _handle_owner($android, $owner_name = NULL) {
  $error = FALSE;
  $owner_uid = NULL;
  if ($owner_name == NULL) {
    // no owner ? Use the user...
    global $user;
    $owner_uid = $user->uid;
  } else {
    if (strtolower($owner_name) == "anonymous") {
      $owner_uid = 0;
    } else {
      $owner = user_load_by_name($owner_name);

      if ($owner) {
        $owner_uid = $owner->uid;
      } else {
        // failed to load user
        $error = t('Failed to load owner by name @name', array('@name' => $owner_name));
      }
    }
  }

  if ($owner_uid !== NULL) {
    $temp = $android->field_owner;
    if (isset($temp['und'][0]['target_id'])) {
      $owner_id = $temp['und'][0]['target_id'];

      if ($owner_id != $owner_uid) {
        // replace
        $android->field_owner = reference_field($owner_uid);
      }
    } else {
      // replace
      $android->field_owner = reference_field($owner_uid);
    }
    if (!$error) {
      things_save($android);
    }
  } else {
    watchdog('things services', "No owner to update");
  }

  return $error;
}

function _handle_one_identifier($android, $identifier_type, $identifier_token = NULL) {
  $error = FALSE;
  $no_identifier = FALSE;

  if ($identifier_token == NULL) {
    $error = t('Identifier value does not exist');
  } else {
    if (property_exists ($android, $identifier_type)) {
      $temp = $android->$identifier_type;
      if (isset($temp['und'][0]['target_id']) && intval($temp['und'][0]['target_id']) != 0) {
        $android_device_identifier_id = $temp['und'][0]['target_id'];
        $device_id = things_ids_load($android_device_identifier_id);
        if ($device_id) {
          if ($device_id->token !== $identifier_token) {
            $device_id->token = $identifier_token;
            $saved = things_ids_save($device_id);
            if (!$saved) {
              $no_identifier = TRUE;
            }
          }
        } else {
          $no_identifier = TRUE;
        }
      } else {
        $no_identifier = TRUE;
      }
    } else {
      $no_identifier = TRUE;
    }

    if ($no_identifier) {
      // no identifier
      $identifier = _create_identifier($identifier_type, $identifier_token);
      if ($identifier) {
        $android->$identifier_type = reference_field($identifier->things_ids_id);
        $saved = things_save($android);
        if (!$saved) {
          $error = t('Failed to save Android device');
        }
      } else {
        $error = t('Failed to save device identifier');
      }
    }
  }
  return $error;
}

function _create_things($things_type, $name) {
  $saved = FALSE;
  $data = array();
  $data['name'] = $name;
  $data['type'] = $things_type;
  $things = things_create($data);
  watchdog("things services", "things created");
  if ($things) {
    $saved = things_save($things);
    watchdog("things services", "things saved $saved");
  }
  if ($saved) {
    return $things;
  } else {
    return $saved;
  }
}

function _create_identifier($identifier_type, $identifier_token) {
  watchdog("things services", "Creating new identifier from type $identifier_type with token $identifier_token");
  $saved = FALSE;
  $data = array();
  $data['type'] = $identifier_type;
  $data['token'] = $identifier_token;
  $identifier = things_ids_create($data);
  if ($identifier) {
    $saved = things_ids_save($identifier);
  }
  if ($saved) {
    return $identifier;
  } else {
    return $saved;
  }
}

function _things_update($id, $things_name, $owner, $updater, $identifier, $identifier_type) {
  // check things type
  $return = _android_device_update($id, $things_name, $owner, $updater, $identifier, $identifier_type);

  // TODO: think on needed hooks and/or Rules actions
  return $return;
}

function _things_delete($id) {
  $id = intval($id);

  $android = things_load($id);
  if ($android) {
    // Handle identifiers

    _delete_identifier($android, "field_device_id");
    _delete_identifier($android, "field_gcm_id");

    things_delete($android);
  }
}
function _things_retrieve($id) {
  $id = intval($id);

  $android = things_load($id);
  return $android;
}

function _delete_identifier($android, $identifier_type) {
  $temp = $android->$identifier_type;
  if (isset($temp['und'][0]['target_id']) && intval($temp['und'][0]['target_id']) != 0) {
    $android_device_identifier_id = $temp['und'][0]['target_id'];
    $identifier = things_ids_load($android_device_identifier_id);
    if ($identifier) {
      things_ids_delete($identifier);
    }
  }
}

function _things_create( $owner, $things_name, $updater, $identifier, $identifier_type, $type) {

  // check things type
  if ($type === "android_device") {
    $return = _android_device_create($type, $things_name, $owner, $updater, $identifier, $identifier_type);
  } else {
    $return = services_error(t('Things type @type is currently not supported', array('@type' => $type)), 406);
  }

  // TODO: think on needed hooks and/or Rules actions
  return $return;
}

function reference_field($value) {
  return array('und' => array ( 0 => array ( 'target_id' => $value)));
}

?>
